import sys
import os

def main():
    command = sys.argv[1]
    if command == 'init':
        os.mkdir('.git')
        os.makedirs('.git/objects/pack')
        os.makedirs('.git/objects/info')
        os.makedirs('.git/rers/head')
        os.makedirs('.git/refs/tags')
        os.mkdir('.git/info')
        os.mkdir('.git/hooks')
        files = {
            '.git/HEAD':"ref: refs/head/main\n",
            '.git/description':'Unnamed repository; edit this file description to name the repository.\n',
            '.git/config':"[core]\n\trepositoryformatversion = 0\n\tfilemode = true\n\tbare = false\n\tlogallrefupdates = true\n\tignorecase = true\n\tprecomposeunicode = true\n" ,
            '.git/info/exclude':"# git ls-files --others --exclude-from=.git/info/exclude\n# Lines that start with '#' are comments.\n# For a project mostly in C, the following would be a good set of\n\t# exclude patterns (uncomment them if you want to use them):\n# *.[oa]\n# *~\n" 
        }
            
        for filename in files:
            # print(file)
            # print(files[file])
            with open(filename, 'w') as f:
                f.write(files[filename])
        print("Initialized empty Git repository\n")
    else:
        raise RuntimeError(f"Unknown command #{command}")
    

if __name__ == '__main__':
    main()