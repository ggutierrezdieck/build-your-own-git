package git

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

func Init(ctx *cli.Context) error {
	var workingDir string
	var PATHS = [2]string{"objects", "refs"}
	const REFNAME = "main" // TODO: need to load from config

	workingDir = GetGitWD(ctx)

	gitDir := filepath.Join(workingDir, ".git")
	CreateDirectories(gitDir)

	for _, path := range PATHS {
		fullPath := filepath.Join(gitDir, path)
		CreateDirectories(fullPath)
	}

	refContent := []byte("ref: refs/heads/" + REFNAME)
	err := os.WriteFile(filepath.Join(gitDir, "HEAD"), refContent, 0644)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Initialized empty Go-git repository in %s \n", workingDir)
	return nil
}
