# Py-Git

This is a personal implementation of [git](https://git-scm.com/doc) version controll system.

## Investigating git objects

Check file type
`file .git/objects/`

Viewing files and their content
`find -type f .git/objects | while read file; do echo $file | zlib-flate -uncompress | xxd; echo ; done`

Calculating object name
`cat .git/objects/<sha> | zlib-flate -uncompress | sha1sum`
