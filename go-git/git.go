package main

import (
	"go-git/git"
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:        "got-git",
		Usage:       "got-git command [path]",
		Description: "A very basic git implentation in go. Based on the book by James Coglan, Building Git",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "C",
				Value:       "",
				DefaultText: "current working directory",
				Usage:       "Run as if git was started in <path> instead of the current working directory ",
			},
		},
		Commands: []*cli.Command{
			{
				Name:   "init",
				Usage:  "init [path] ",
				Action: git.Init,
			},
			{
				Name:   "commit",
				Usage:  "commit [path] ",
				Action: git.Commit,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
