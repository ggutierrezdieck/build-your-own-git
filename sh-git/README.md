# Manual init repository 

This is a repository created manual folder by folder, file by file using shell commands.

## Steps
### git init
The minimal steps required to initialized a repository.

1. Create .git directory
`mkdir .git`
1. Create .git/HEAD file
echo 'ref: refs/heads/main' > .git/HEAD
1. Create ojects and refs directory
`mkdir -p .git/objects/ .git/refs`

At this stage the directory is a valid git repository. Extra directories and files created by git.

**Directories**
hooks
info
objects/info
objects/pack
ref/heads
fef/tags

**Files**
config
descrption
info/exclude
hooks/applypatch-msg.sample
hooks/commit-msg.sample
hooks/fsmonitor-watchman.sample
hooks/post-update.sample
hooks/pre-applypatch.sample
hooks/pre-commit.sample
hooks/pre-merge-commit.sample
hooks/pre-push.sample
hooks/pre-rebase.sample
hooks/pre-receive.sample
hooks/prepare-commit-msg.sample
hooks/push-to-checkout.sample
hooks/update.sample

### git commit
Creating a commit manually using shell commands.q

1. Create first file
`echo "# Manual repo \n This is a repository created manual folder by folder, file by file." > README.md`
1. Calculate file sha
`echo "blob "`wc -c <filename>| awk '{print $1}'`"\u0001"`cat <filename>` |shasum`


----
git add = creates entry in index and creates object in db
git commit = creates ref/heads/main, logs/HEAD and log Refs/heads/main,	COMMIT_EDITMSG and tree commit objects

Tested commands
- create sha
echo "blob "`wc -c one| awk '{print $1}'`"\u0000"`cat one` |shasum
- compress file 
cat one  | python2 -c 'import sys,zlib; sys.stdout.write(zlib.compress(sys.stdin.read()))' > compressfil
cat one | zlib-flate -compress=1 > compres
- Compress and uncompress file
cat one | zlib-flate -compress=1 |  zlib-flate -uncompress
deflate file 

