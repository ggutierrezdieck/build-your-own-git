package git

import (
	"log"
	"os"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

func GetGitWD(ctx *cli.Context) string {
	path := ctx.String("C")
	if IsValidPath(path) {
		if path != "" {
			absPath, err := filepath.Abs(ctx.String("C"))
			if err != nil {
				log.Panic(err)
			}
			return absPath
		} else {
			currentDirAbsPath, err := os.Getwd()
			if err != nil {
				log.Fatal(err)
			}
			return currentDirAbsPath
		}
	}
	return ""
}

func CreateDirectories(path string) {

	err := os.Mkdir(path, 0750)
	if err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

}

func IsValidPath(path string) bool {
	_, err := os.Stat(path)

	if err != nil {
		log.Fatalf("fatal: cannot change to '%s': No such file or directory", path)
	}
	return true

}

func IsValidRepo(ctx *cli.Context) bool {
	workingDir := GetGitWD(ctx)
	const REFNAME = "main" // TODO: need to load from config
	if IsValidPath(workingDir) {

		gitDir := filepath.Join(workingDir, ".git")
		HEADfile := filepath.Join(gitDir, "HEAD")
		var paths = [2]string{"objects", "refs"}

		for _, path := range paths {
			fullPath := filepath.Join(gitDir, path)

			_, err := os.Stat(fullPath)
			if err != nil {
				log.Fatalf("fatal: not a git repository: .git")
			}
		}

		_, err := os.ReadFile(HEADfile)
		if err != nil {
			log.Printf("fatal: not a git repository: .git")
			log.Fatal(err)
		}

	}
	return true

}
