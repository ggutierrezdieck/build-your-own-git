#!/bin/bash

usage(){
    # TODO
    echo 'Implement usage'
}

case "$1" in
    "init")
        init(){
            mkdir -p .git/objects/pack .git/objects/info .git/refs/heads .git/refs/tags .git/info .git/hooks 
            printf "ref: refs/head/main\n" > .git/HEAD
            printf "Unnamed repository; edit this file 'description' to name the repository.\n" > .git/description 
            printf "[core]\n\trepositoryformatversion = 0\n\tfilemode = true\n\tbare = false\n\tlogallrefupdates = true\n\tignorecase = true\n\tprecomposeunicode = true\n" > .git/config
            printf "# git ls-files --others --exclude-from=.git/info/exclude\n# Lines that start with '#' are comments.\n# For a project mostly in C, the following would be a good set of\n\t# exclude patterns (uncomment them if you want to use them):\n# *.[oa]\n# *~\n" > .git/info/exclude
        }
        if [ ! -d .git ]
        then
            init
            printf "Initialized empty Git repository in /private/tmp/test/.git/\n"
        else
            init
            printf "Reinitialized existing Git repository in /Users/ggutierrezdieck/personal/local-projects/gits/.git/"
        fi
    ;;

    "cat-file")
        case "$2" in
        -p) # Read object, it can read if object si provided, or the path format
            
            object_name=$(echo -n "$3" | cut -c3-)
            object=$(find . -path "*$object_name*")
            if [ -n "$object" ] 
            then 
                zlib-flate -uncompress < "$object"
            else
                echo "fatal: Not a valid object name $object"
            fi
    
        ;;
        *)
            # TODO
            echo 'cat-file usage'
        ;;
        esac
    ;;
    
    *)
        usage
    ;;
esac

